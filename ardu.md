
Arduino tahák
=============

Teorie
------

Úplně na začátku se můžou vytvořit proměnné: `<typ> <název> = <hodnota>;`.

```cpp
int led = 12;
```
Pokud víme, že hodnotu proměnné nebudeme během chodu programu měnit, může použít slovo `const`, což znamená, že proměnná bude konstantní (jakmile jí nastavíme hodnotu, už nepůjde přenastavit na jinou).

```cpp
const int sensor = 9;
```
Potom lze všude v kódu místo čísla `12` psát název proměnné `led` a program se bude chovat stejně, jako bychom ručně napsali všude číslo `12`.

Ve funkci `setup` se nastaví, jestli je pin vstupní (`INPUT`) nebo výstupní (`OUTPUT`) pomocí `pinMode(<číslo pinu>, <způsob>);`.

```cpp
pinMode(12, OUTPUT);
```

Ve funkci `loop` se vše děje stále dokola.

Poslání `HIGH` nebo `LOW` na výstup např. pro rozsvícení ledky: `digitalWrite(<číslo pinu>, HIGH);`.

Posláním `HIGH` na ledku jí zapneme a posláním `LOW` vypneme.

```cpp
digitalWrite(led, HIGH);
```

Poslání konkrétní hodnoty na výstup např. pro ovládání jasu ledky: `analogWrite(<číslo pinu>, <hodnota>);`.

Lze poslat hodnoty od `0` do `255`.

```cpp
analogWrite(led, 127);
```

Podmínka `if` mění chování programu podle toho, zda je testovaná vlastnost splněná či nikoli. V závorce po slově `if` následuje podmínka, za ní následuje ve složených závorkách kód, který s vykoná, pokud je podmínka splněná a poté následuje část `else`, která je nepovinná a za ní je opět ve složených závorkách kód, který se vykoná, pokud podmínka splněná není.

testování rovnosti: `<něco> == <něco jiného>`

porovnání velikosti: `>`, `>=`, `<=`, `<`

```cpp
if (input > 0) {
    digitalWrite(led, HIGH);
}
```

```cpp
if (input == 1023) {
    digitalWrite(led, HIGH);
} else {
    digitalWrite(led, LOW);
}
```

čtení `HIGH` nebo `LOW` např. z tlačítka: `digitalRead(<číslo pinu>)`

Tato funkce za sebe vrací hodnotu (stejně jako proměnné). vrátí buď `HIGH` nebo `LOW` a její hodnotu lze například ukládat do proměnné nebo testovat v podmínce.

```cpp
input = digitalRead(9);
```

```cpp
if (digitalRead(button) == HIGH) {
    analogWrite(led, 127);
}
```

Do proměnné `button` si zde nejprve musíme uložit číslo pinu: `const int button = <číslo pinu>`.

čtení konkrétní hodnoty např. z potenciometru: `analogRead(<číslo pinu>)`

Funkce za sebe vrací číslo od `0` do `1023`.

```cpp
input = analogRead(sensor);
```

Maximální hodnota může být až `1024`, ale na výstup můžeme poslat maximálně `255`, takže než číslo načtené z potenciometru můžeme poslat na výstup, musíme ho pokrátit

```cpp
x = input / 4;
```

Potom už můžeme číslo například poslat do ledky.

```cpp
analogWrite(led, x);
```

Analogové vstupy, jako je např. potenciometr, je nutné zapojit do pinů začínajících na `A` např. `A5`.

Čekání: `delay(<počet milisekund>)`

1 sekunda = 1000 milisekund

```cpp
delay(2000)
```

Příklady
--------
```cpp
//TLAČÍTKO A LEDKA
int led = 11;
int button = 12; 

void setup() {
  pinMode(led, OUTPUT);
  pinMode(button, INPUT);
}

void loop() {
  if (digitalRead(button) == LOW) {
    digitalWrite(led, LOW);
  } else {
    digitalWrite(led, HIGH);
  }
}
```

```cpp
//SEMAFOR
int red = 9;
int yellow = 10;
int green = 11;

void setup() {
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
}

void loop() {
  digitalWrite(red, HIGH);
  delay(2000);
  digitalWrite(yellow, HIGH);
  delay(1000);
  digitalWrite(red, LOW);
  digitalWrite(yellow, LOW);
  digitalWrite(green, HIGH);
  delay(2000);
  digitalWrite(green, LOW);
  digitalWrite(yellow, HIGH);
  delay(1000);
  digitalWrite(yellow, LOW);
}
```

```cpp
//RGB
const int led1 = 9;
const int led2 = 10;
const int led3 = 11;
const int sensor = A5;
int value = 0;
int color1 = 0;
int color2 = 0;
int color3 = 0;

void setup() {
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(sensor, INPUT);
}

void loop() {
  value = analogRead(sensor);
  color1 = 255 - (value % 256);
  color2 = 255 - ((value / 2) % 256);
  color3 = 255 - (value / 4);
  analogWrite(led1, color1);
  analogWrite(led2, color2);
  analogWrite(led3, color3);
}
```
