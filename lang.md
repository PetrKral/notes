
Programovací jazyky
===================

Python
------

```python
def factorial(number):
    if number == 0:
        return 1
    else:
        return number * factorial(number - 1)

print(factorial(5))
```

Javascript
----------

```js
function factorial(number) {    
    if (number == 0) {
        return 1
    } else {
        return number * factorial(number - 1)
    }
}

console.log(factorial(5))
```

C++
---

```cpp
#include <iostream>

uint factorial(uint number) {
    if (number == 0) {
        return 1;
    } else {
        return number * factorial(number - 1);
    }
}

int main() {
    std::cout << factorial(5) << '\n';
}
```

Go
--

```go
package main

import "fmt"

func factorial(number int) int {
    if (number == 0) {
        return 1
    } else {
        return number * factorial(number - 1)
    }
}

func main() {
    fmt.Println(factorial(5))
}
```

Rust
----

```rust
fn factorial(number:i64) -> i64 {
    if number == 0 {
        return 1;
    } else {
        return number * factorial(number - 1);
    }
}

fn main() {
    println!("{}", factorial(5));
}
```
